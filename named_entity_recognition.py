"""
Programming task
================

The following is an implementation of a simple Named Entity Recognition (NER).
NER is concerned with identifying place names, people names or other special
identifiers in text.

Here we make a very simple definition of a named entity: A sequence of
at least two consecutive capitalized words. E.g. "Los Angeles" is a named
entity, "our hotel" is not.

While the implementation passes the Unit test, it suffers from bad structure and
readability. It is your task to rework *both* the implementation and the Unit
test. You are expected to come up with a better interface than the one presented
here.

Your code will be evaluated on:
- Readability: Is naming intuitive? Are there comments where necessary?
- Structure: Is functionality grouped into functions or classes in a way that
enables reusability?
- Testability: Is it easy to test individual components of your algorithm? This
is a good indicator of good interface design.
- Bonus: Functional programming. Demonstrate how you have applied principles of
functional programming to improve this code.

If you want, explain reasons for changes you've made in comments.

Note that you don't have to improve the actual Named Entity Recognition
algorithm itself - the focus is on code quality.
"""


import re
import unittest
from itertools import groupby, compress


class NamedEntityRecognition():
    """Class performing the named entity recognition (NER)"""
    """Laszlo Fesus <xtry1987@gmail.com>"""

    """ A class for performing named entity recognition on text.
        [Organized required methods in a class so that we can hide implementation
        details and provide a single interface (here via the method recognize_entities()) ]
    """

    """
    Constructor for new instances of NamedEntityRecognition.
    @param text: (String) The text to perform entity recognition on
    @param tokenizer: (Regex) A regex that matches tokens and is used to tokenize the text into a list of tokens
    @param matcher: (Regex) A regex that matches (part of) the entity we look for
    @param num_elems: (Int, defaults to 2 as required by task) The number of subsequent matches required for the entity
    """

    def __init__(self, text, tokenizer, matcher, num_elems=2):

        self._text = text
        self._tokenizer = tokenizer
        self._matcher = matcher
        self._num_elems = num_elems

    """
    Perform entity recognition on the instance and return the set of matched entities.
    This function tokenizes the text and generates lists of matching sequences. It then returns
    a set of matching strings, constructed from the matching sequences, for those sequences that
    exactly match the required number of elements (self._num_elems).
    Example:
        "When we went to Los Angeles last year we visited the Hollywood Sign"
         -> {'Hollywood Sign', 'Los Angeles'}
         (for _num_elems = 2, self._tokenizer = re.compile(r"([a-z]+)\s*(.*)$", re.I), self._matcher = re.compile(r"[A-Z][a-z]*$"))
    @param matches: List of Lists, each inner list contains a sequence of matching tokens
    @return: Set containing joined matching tokens
    [Provide this public method such that we can hide the steps in the computation; the method just
    calls the relevant functions, each of which basically performs a single task]
    """

    def recognize_entities(self):

        # tokenize the text
        tokens = self._tokenize()

        # generate a list of list of matches
        matches = self._extract_subsequent_matches(tokens)

        # apply post-filtering and formatting
        processed_matches = self._post_process_matches(matches)

        return processed_matches

    """
    Tokenize the text using tokenizer
    Example: "This is the text" -> ['This', 'is', 'the', 'text']
        (for _self.tokenizer re.compile(r"([a-z]+)\s*(.*)$", re.I))
    Implementation assumption: We take both regular expressions as given.
    @return: Array of tokens resulting from the application of tokenizer to the text.
    [This part is a refactoring and decoupling. The refactoring makes the code shorter, the decoupling
    improves the separation of tasks and testability]
    """

    def _tokenize(self):

        # create local copy for tokenizer consumption and initialize tokens
        # list
        text, tokens = (self._text, [])
        # apply tokenizer to text
        token_match = self._tokenizer.match(text)
        while token_match:
            tokens.append(token_match.group(1))
            text = token_match.group(2)
            token_match = self._tokenizer.match(text)

        return tokens

    """
    Extract list of elements that match in sequence using _matcher for matching
    Example:
        ['When', 'we', 'went', 'to', 'Los', 'Angeles', 'last', 'year', 'we', 'visited', 'the', 'Hollywood', 'Sign']
         -> [['When'], ['Los', 'Angeles'], ['Hollywood', 'Sign']]
         (for self._matcher = re.compile(r"[A-Z][a-z]*$"))
    @param tokens: List, tokens to perform matching on
    @return: List of Lists, each inner list contains a sequence of matching tokens (possibly degenerate)
    """

    def _extract_subsequent_matches(self, tokens):

        return [list(y) for x, y in groupby(tokens, lambda a: bool(self._matcher.match(a))) if x is True]

    """
    Return a processed and formatted list by
    - filtering on the number of items that match in a sequence (_num_elems)
    - joining the inner lists to strings
    - creating a set to ensure uniqueness
    Example:
        [['When'], ['Los', 'Angeles'], ['Hollywood', 'Sign']]
         -> {'Hollywood Sign', 'Los Angeles'}
         (for _num_elems = 2)
    @param matches: List of Lists, each inner list contains a sequence of matching tokens
    @return: Set containing joined matching tokens
    """

    def _post_process_matches(self, matches):

        return set([' '.join(match) for match in matches if len(match) == self._num_elems])


class NamedEntityRecognitionTestCases(unittest.TestCase):

    """[Added test cases for all the separate tasks the class methods are supposed to perform to
        ensure that each piece works as expected. Test cases contain standard cases as well as some
        corner cases.]"""

    def setUp(self):
        # Regular expression for matching a token at the beginning of a
        # sentence
        self.token_re = re.compile(r"([a-z]+)\s*(.*)$", re.I)

        # Regular expression to recognize an uppercase token
        self.uppercase_re = re.compile(r"[A-Z][a-z]*$")

    """ Set of tests for the _tokenize-method """

    def test_tokenize_standard_case(self):
        # Test standard case of sentence without punctuation
        text = "When we went to Los Angeles last year we visited the Hollywood Sign"
        ne = NamedEntityRecognition(
            text, self.token_re, self.uppercase_re, num_elems=2)
        self.assertEqual(ne._tokenize(), ['When', 'we', 'went', 'to', 'Los',
                                          'Angeles', 'last', 'year', 'we', 'visited', 'the', 'Hollywood', 'Sign'])

    def test_tokenize_corner_case_empty(self):
        # Test corner case of empty string. Expected behavior is returning an
        # empty list.
        text = ""
        ne = NamedEntityRecognition(
            text, self.token_re, self.uppercase_re, num_elems=2)
        self.assertEqual(ne._tokenize(), [])

    def test_tokenize_corner_case_sentence(self):
        # Test corner case of sentence with punctuation. Expected behavior is returning only matches up to
        # the first punctuation sign. (as in the original example)
        text = "When we went to Los Angeles! last year we visited the Hollywood Sign"
        ne = NamedEntityRecognition(
            text, self.token_re, self.uppercase_re, num_elems=2)
        self.assertEqual(ne._tokenize(), [
                         'When', 'we', 'went', 'to', 'Los', 'Angeles'])

    """ Set of test for the _extract_subsequent_matches-method """

    def test_extract_subsequent_matches_standard_case(self):
        # Test standard case of list with several elements
        arr = ['When', 'we', 'went', 'to', 'Los', 'Angeles', 'last',
               'year', 'we', 'visited', 'the', 'Hollywood', 'Sign']
        ne = NamedEntityRecognition(
            "", self.token_re, self.uppercase_re, num_elems=2)
        self.assertEqual(ne._extract_subsequent_matches(arr), [
                         ['When'], ['Los', 'Angeles'], ['Hollywood', 'Sign']])

    def test_extract_subsequent_matches_corner_case_empty(self):
        # Test corner case of empty list. Expected behavior is returning an
        # empty list.
        arr = []
        ne = NamedEntityRecognition(
            "", self.token_re, self.uppercase_re, num_elems=2)
        self.assertEqual(ne._extract_subsequent_matches(arr), [])

    """ Set of tests for _post_process_matches-method """

    def test_post_process_matches(self):
        # Test standard case of list with lists of various sizes
        ne = NamedEntityRecognition(
            "", self.token_re, self.uppercase_re, num_elems=2)
        arr = [['When'], ['Los', 'Angeles'], ['Hollywood', 'Sign']]
        self.assertEqual(ne._post_process_matches(arr), {
                         'Hollywood Sign', 'Los Angeles'})

    def test_post_process_matches_corner_case_empty_input(self):
        # Test corner case of empty input list. Expected behavior is returning
        # an empty set.
        ne = NamedEntityRecognition(
            "", self.token_re, self.uppercase_re, num_elems=2)
        arr = []
        #self.assertEqual(ne._post_process_matches(arr), set())

    def test_post_process_matches_corner_case_empty_output(self):
        # Test corner case of empty output (num_elems = 3). Expected behavior
        # is returning an empty set.
        ne = NamedEntityRecognition(
            "", self.token_re, self.uppercase_re, num_elems=3)
        arr = [['When'], ['Los', 'Angeles'], ['Hollywood', 'Sign']]
        #self.assertEqual(ne._post_process_matches(arr), set())

    """ Set of tests for the (public) recognize_entities-method """

    def test_recognize_entities_standard_case_one(self):
        # Test standard case of sentence without punctuation and sequences of
        # length one
        text = "When we went to Los Angeles last year we visited the Hollywood Sign"
        ne = NamedEntityRecognition(
            text, self.token_re, self.uppercase_re, num_elems=1)
        self.assertEqual(ne.recognize_entities(), set(["When"]))

    def test_recognize_entities_standard_case_two(self):
        # Test standard case of sentence without punctuation and sequences of
        # length two
        text = "When we went to Los Angeles last year we visited the Hollywood Sign"
        ne = NamedEntityRecognition(
            text, self.token_re, self.uppercase_re, num_elems=2)
        self.assertEqual(ne.recognize_entities(), set(
            ["Los Angeles", "Hollywood Sign"]))

    def test_recognize_entities_corner_case_empty_string(self):
        # Test standard case for empty input string. Expected behavior is
        # returning an empty set
        text = ""
        ne = NamedEntityRecognition(
            text, self.token_re, self.uppercase_re, num_elems=2)
        self.assertEqual(ne.recognize_entities(), set())

    def test_recognize_entities_corner_case_empty_output(self):
        # Test standard case for empty output set (no matches of desired
        # length). Expected behavior is returning an empty set
        text = "When we went to Los Angeles last year we visited the Hollywood Sign"
        ne = NamedEntityRecognition(
            text, self.token_re, self.uppercase_re, num_elems=3)
        self.assertEqual(ne.recognize_entities(), set())


if __name__ == "__main__":
    unittest.main()
